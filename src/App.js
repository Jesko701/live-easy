import "./App.css";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import { useState } from "react";
import { AuthContext } from "./Auth/Auth";
import { PrivateRoute } from "./Auth/PrivateRoute";
import { RestrictedRoute } from "./Auth/PrivateRoute";
import DashboardPages from "./pages/DashboardPages";
import LoginPages from "./pages/LoginPages";
import EditItemPages from "./pages/EditItemPages";
import AddItemPages from "./pages/AddItemPages";
import NotFoundPages from "./pages/NotFoundPages";
import CekSpin from "./pages/tesSpinner";
import RegisterPage from "./pages/RegisterPages";

function App() {
  const isAnyToken = JSON.parse(localStorage.getItem("tokens"));
  const [authToken, setAuthToken] = useState(isAnyToken);

  const setAndGetTokens = (token) => {
    localStorage.setItem("tokens", JSON.stringify(token));
    setAuthToken(token);
  };

  return (
    <AuthContext.Provider value={{ authToken, setAndGetTokens }}>
      <BrowserRouter>
        <Routes>
          <Route path="spin" element={<CekSpin />} />
          <Route
            path="/"
            element={
              <RestrictedRoute>
                <LoginPages />
              </RestrictedRoute>
            }
          />
          <Route
            path="register"
            element={
              <RestrictedRoute>
                <RegisterPage />
              </RestrictedRoute>
            }
          />
          <Route
            path="item"
            element={
              <PrivateRoute>
                <DashboardPages />
              </PrivateRoute>
            }
          />
          <Route
            path="add"
            element={
              <PrivateRoute>
                <AddItemPages />
              </PrivateRoute>
            }
          />
          <Route
            path="item/:id"
            element={
              <PrivateRoute>
                <EditItemPages />
              </PrivateRoute>
            }
          />
          <Route path="*" element={<NotFoundPages />} />
        </Routes>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
