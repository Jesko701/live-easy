import React from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "./Auth";

export const PrivateRoute = ({ children }) => {
  const { authToken } = useAuth();

  return authToken ? children : <Navigate to="/" />;
};

export const RestrictedRoute = ({ children }) => {
  console.log("tres");
  const { authToken } = useAuth();

  return authToken ? <Navigate to={-1} /> : children;
};
