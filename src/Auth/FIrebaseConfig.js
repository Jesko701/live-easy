import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { useAuth } from "./Auth";

const firebaseConfig = {
  apiKey: "AIzaSyA5yjquuaPn4E3xU11c8knJXRvKY9uMKjQ",
  authDomain: "fir-react-tutorial-10d0b.firebaseapp.com",
  projectId: "fir-react-tutorial-10d0b",
  storageBucket: "fir-react-tutorial-10d0b.appspot.com",
  messagingSenderId: "979903977980",
  appId: "1:979903977980:web:a3edc2fa743724e7057a79",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

const provider = new GoogleAuthProvider();

export const signInWithGoogle = async () => {
  try {
    const result = await signInWithPopup(auth, provider);
    console.log(result.user.accessToken);
    const response = await fetch(
      "http://116.193.191.1:54321/api/v1/auth/login/google",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firebaseJWT: result.user.accessToken,
        }),
      }
    );
    const data = await response.json();
    console.log(data);
    return data.data.token
  } catch (error) {
    console.log(error);
  }
};
 