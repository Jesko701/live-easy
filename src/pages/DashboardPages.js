import { useState } from "react";
import { useEffect } from "react";
import { useAuth } from "../Auth/Auth";
import { api } from "../components/config";
import ListData from "../components/ListData";
import Navbar from "../components/Navbar";
const DashboardPages = () => {
  const {authToken} = useAuth();
  const [user,setUser] = useState({});
  const getUserApi = async () => {
    try {
      const fetchUser = await api.get("api/v1/user/profile", {
        headers: {
          Authorization: "Bearer " + authToken,
        },
      });
      setUser(fetchUser.data.data)
      console.log(user);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getUserApi();
  },[]);
  return (
    <>
      <Navbar data={user}/>
      <ListData />
    </>
  );
};

export default DashboardPages;
