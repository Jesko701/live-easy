import { useAuth } from "../Auth/Auth";
import { useProfileContext } from "../Auth/ProfileRoute";
import Header from "../components/Header";
import Login from "../components/Login";
const LoginPages = () => {
  const containerStyle = {
    background: "#fbfbfb",
    padding: "100px",
    width: "50%",
    paddingTop: "5px",
    boxShadow: "2px 2px 3px 0px",
    marginLeft: "auto",
    marginRight: "auto",
    fontFamily: "Poppins, sans-serif",
  };

  const center = {
    ...containerStyle,
  };


  return (
    <div className="container" style={center}>
      <Header></Header>
      <Login></Login>
    </div>
  );
};

export default LoginPages;
