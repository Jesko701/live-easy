import notFound from "../components/assets/img/404.svg"

const NotFoundPages = () => {
    return (
        <div style={{textAlign:"center", marginTop: "10rem" }}>
            <img src={notFound} style={{width:"75%"}}/>
        </div>
    )
}

export default NotFoundPages