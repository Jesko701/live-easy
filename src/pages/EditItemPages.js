import EditItem from "../components/EditItem";
import Header from "../components/Header";

const EditItemPages = () => {
  const containerStyle = {
    background: "#fbfbfb",
    padding: "100px",
    width: "50%",
    paddingTop: "5px",
    boxShadow: "2px 2px 3px 0px",
    marginLeft: "auto",
    marginRight: "auto",
  };
  return (
    <div className="container" style={containerStyle}>
      <Header />
      <EditItem />
    </div>
  );
};

export default EditItemPages;
