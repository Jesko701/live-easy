import Header from "../components/Header";
import AddItem from "../components/AddItem";


const AddItemPages = () => {
  const containerStyle = {
    background: "#fbfbfb",
    padding: "100px",
    width: "50%",
    paddingTop: "5px",
    boxShadow: "2px 2px 3px 0px",
    marginLeft: "auto",
    marginRight: "auto",
  };
  return (
    <div className="container" style={containerStyle}>
      <Header />
      <AddItem />
    </div>
  );
};

export default AddItemPages;
