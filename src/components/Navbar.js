import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../Auth/Auth";
import Spinner from "./Spinner";

const Navbar = ({ data }) => {
  const { setAndGetTokens } = useAuth();
  const [isSpin, setIsSpin] = useState(false);
  const handleLogout = () => {
    setIsSpin(true);
    setTimeout(() => {
      alert("Berhasil Log Out");
      setAndGetTokens();
      localStorage.clear();
    }, 1000);
  };
  const loading = () => {
    if (isSpin) return <Spinner />;
  };
  useEffect(() => {
    console.log(data.email);
  }, []);
  return (
    <>
      {loading()}
      <nav class="navbar navbar-dark navbar-expand-md bg-dark py-3">
        <div class="container">
          <a class="navbar-brand d-flex align-items-center" href="#">
            <span class="bs-icon-sm bs-icon-rounded bs-icon-primary d-flex justify-content-center align-items-center me-2 bs-icon">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="1em"
                height="1em"
                fill="currentColor"
                viewBox="0 0 16 16"
                class="bi bi-person-circle"
              >
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"></path>
                <path
                  fill-rule="evenodd"
                  d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
                ></path>
              </svg>
            </span>
            <span>{data.email}</span>
          </a>
          <button
            data-bs-toggle="collapse"
            class="navbar-toggler"
            data-bs-target="#navcol-5"
          >
            <span class="visually-hidden">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navcol-5">
            <ul class="navbar-nav ms-auto">
              <li class="nav-item"></li>
              <li class="nav-item"></li>
              <li class="nav-item">
                <Link style={{ color: "white" }} to="/add">
                  Add Item
                </Link>
              </li>
            </ul>
            <button
              class="btn btn-primary ms-md-2"
              role="button"
              onClick={handleLogout}
            >
              Logout
            </button>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
