import React, { useEffect, useRef, useState } from "react";
import Spinner from "./Spinner";
import Google from "./assets/img/google-color-icon.svg";
import { Link, useNavigate } from "react-router-dom";
import { api } from "./config";
import { useAuth } from "../Auth/Auth";
import { signInWithGoogle } from "../Auth/FIrebaseConfig";

const Login = () => {
  const [isSpin, setIsSpin] = useState(false);
  const [errMsg, setErrMsg] = useState("");
  const [login, setLogin] = useState("Login");
  const navigate = useNavigate();

  const { setAndGetTokens } = useAuth();

  let user = {};
  const var_email = useRef();
  const var_pw = useRef();

  const submitHandler = async () => {
    try {
      setIsSpin(true);
      const login = await api.post("api/v1/auth/login", {
        email: var_email.current.value,
        password: var_pw.current.value,
      });
      setAndGetTokens(login.data.data.token);
      console.log(user);
      navigate("/item", { state: { user } });
    } catch (error) {
      console.log(error);
      setErrMsg(error.response.data.data);
    }
    setTimeout(() => {
      setIsSpin(false);
    }, 1000);
  };

  const showFeedback = () => {
    if (isSpin) return <Spinner />;
    else if (errMsg != "")
      return (
        <div className={`alert alert-danger text-center`} role="alert">
          {errMsg}
        </div>
      );
  };

  // * Sign in with google
  const loginGoogle = async () => {
    const token = await signInWithGoogle();
    setAndGetTokens(token);
    navigate("/item");
  };

  const inputStyle = {
    minWidth: "100%",
    borderRadius: "20px",
    height: "30px",
    padding: "10px",
  };
  const fontStyle = {
    fontSize: "18px",
    fontWeight: "bold",
    fontFamily: "Poppins,sans-serif",
  };

  const fontStyleEnd = {
    fontSize: "20px",
    fontFamily: "Poppins, sans-serif",
    textAlign: "center",
    marginTop: "17px",
  };

  return (
    <React.Fragment>
      {showFeedback()}
      <div id="email" style={{ marginBottom: "1rem" }}>
        <p style={fontStyle}>Email</p>
        <input type="email" style={inputStyle} ref={var_email} />
      </div>
      <div id="password">
        <p style={fontStyle}>Password</p>
        <input type="password" style={inputStyle} ref={var_pw} />
      </div>
      <div id="button" style={{ textAlign: "center", marginTop: "15px" }}>
        <button
          onClick={submitHandler}
          class="btn btn-primary"
          type="button"
          style={{ minWidth: "119px", minHeight: "50px" }}
        >
          <Link
            style={{
              fontSize: "15px",
              background: "#666fcf",
              fontFamily: "Poppins, sans-serif",
              fontWeight: "bold",
              color: "white",
              borderRadius: "5px",
            }}
          >
            {login}
          </Link>
          <br />
        </button>
      </div>
      <div id="not-have-account" style={fontStyleEnd}>
        <p>
          Don't have an account?
          <Link
            style={{ color: "rgb(0,103,255)", fontSize: "20px" }}
            to="/register"
          >
            Register Here
          </Link>
          ={" "}
        </p>
        <p>or</p>
        <p>Sign in With Google</p>
        <a onClick={loginGoogle}>
          <img src={Google} width="49" height="49" />
        </a>
      </div>
    </React.Fragment>
  );
};

export default Login;
