import { api } from "./config";
import React, { useRef, useState } from "react";
import Spinner from "./Spinner";
import { useNavigate } from "react-router-dom";
const Register = () => {
  let email = useRef(null);
  let name = useRef(null);
  let password = useRef(null);
  let confirm_password = useRef(null);

  const navigate = useNavigate();
  const [isLoading, setLoading] = useState(false);
  const [dataKaryawan, set_dataKaryawan] = useState({
    email: "",
    password: "",
    name: "",
  });
  const [errMsg,setErrMsg] = useState('');

  const handleInput = () => {
    let data = dataKaryawan;
    data["email"] = email.current.value;
    data["password"] = password.current.value;
    data["name"] = name.current.value;
    set_dataKaryawan(data);
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    handleInput();
    console.log(dataKaryawan);

    if (dataKaryawan.password !== confirm_password.current.value) {
      alert("Password and confirm password doesn't match");
      return;
    }
    setLoading(true);
    try {
      const register = await api.post(
        "/api/v1/auth/register",
        { ...dataKaryawan }
      );
      console.log(register);
      alert(`${dataKaryawan.email} berhasil didaftar`);
      navigate("/login", { replace: true });
    } catch (error) {
      console.log(error.response);
      setErrMsg(error.response.data.data)
    }
    setTimeout(()=>{
      setLoading(false);
    },1000)
  };

  const inputStyle = {
    minWidth: "100%",
    borderRadius: "20px",
    height: "30px",
    padding: "10px",
  };
  const fontStyle = {
    fontSize: "18px",
    fontWeight: "bold",
    fontFamily: "Poppins,sans-serif",
  };

  const showFeedback = () => {
    if (isLoading) return <Spinner />;
    else if (errMsg !== "")
      return (
        <div className={`alert alert-danger text-center`} role="alert">
          {errMsg}
        </div>
      );
  };

  return (
    <>
      {showFeedback()}
      <div id="email" style={{ marginBottom: "1rem" }}>
        <p style={fontStyle}>Nama</p>
        <input type="text" style={inputStyle} ref={name} />
      </div>
      <div id="email" style={{ marginBottom: "1rem" }}>
        <p style={fontStyle}>Email</p>
        <input type="email" style={inputStyle} ref={email} />
      </div>
      <div id="password" style={{ marginBottom: "1rem" }}>
        <p style={fontStyle}>Password</p>
        <input type="password" style={inputStyle} ref={password} />
      </div>
      <div id="password" style={{ marginBottom: "1rem" }}>
        <p style={fontStyle}>Confirm Password</p>
        <input type="password" style={inputStyle} ref={confirm_password} />
      </div>
      <div id="button" style={{ textAlign: "center", marginTop: "15px" }}>
        <button
          class="btn btn-primary"
          type="button"
          style={{
            fontSize: "15px",
            minWidth: "119px",
            minHeight: "50px",
            background: "#666fcf",
            fontFamily: "Poppins, sans-serif",
            fontWeight: "bold",
            color: "white",
            borderRadius: "5px",
          }}
          onClick={submitHandler}
        >
          Register
          <br />
        </button>
      </div>
    </>
  );
};

export default Register;
