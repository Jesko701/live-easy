import Logo from "./assets/img/Live Easy Logo.png";

const Header = () => {
  const sytlingLogo = {
    textAlign: "center",
    marginTop: "1rem",
  };
  return (
    <div style={sytlingLogo}>
      <img src={Logo} width="115" height="79" />
      <div id="live-easy" style={{ marginTop: "1rem" }}>
        <div class="row">
          <div
            class="col-md-6"
            style={{
              textAlign: "right",
              display: "inline",
              position: "relative",
            }}
          >
            <p
              style={{
                borderRadius: "15px",
                padding: "10px",
                color: "#666fcf",
                fontSize: "3rem",
                maxWidth: "fit-content",
                display: "inline",
                padding: "5px",
                fontFamily: "Poppins, sans-serif",
              }}
            >
              <strong>LIVE</strong>
            </p>
          </div>
          <div class="col">
            <p
              style={{
                fontSize: "2rem",
                textAlign: "left",
                background: "#ff2539",
                fontFamily: "Poppins, sans-serif",
                borderRadius: "15px",
                color: "white",
                maxWidth: "fit-content",
                padding: "10px",
                fontWeight: "bold",
                fontFamily: "Poppins, sans-serif",
              }}
            >
              EASY
            </p>
          </div>
        </div>
        <div class="row" id="Slogan">
          <div class="col" style={{ textAlign: "center" }}>
            <p style={{ fontSize: "25px", color: "#9d9d9d" }}>
              Your healthcare partner solution
              <br />
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
