import React from "react";
import { useState } from "react";
import { api } from "./config";
import { useAuth } from "../Auth/Auth";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const ListData = () => {
  const imagePositionStyle = {
    textAlign: "center",
    objectFit: "contain",
    borderStyle: '2px solid'
  };
  const imageStyle = {
    width: "70%",
    height: "50%",
    borderRadius: '20px',
    border: '2px solid blue',
    marginTop: '5%',
  };
  const paragraphStyle = {
    margin: "10px",
  };
  const titleMedicineStyle = {
    ...paragraphStyle,
    fontWeight: "bold",
  };

  const columnStyle = {
    textAlign: "left",
    borderStyle: "outset",
    borderRadius: '10px',
    backgroundColor: "#f5f5f5",
    marginRight: '5%'
  }

  const { authToken } = useAuth();
  const [dataMeds, setDataMed] = useState([]);
  const navigate = useNavigate();

  const listing = async () => {
    try {
      const list = await api.get(`/api/v1/medicine`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
          "Content-Type": "multipart/form-data",
        },
      });
      // * Data seharusnya sudah masuk
      setDataMed(list.data.data);
      console.log(dataMeds);
      console.log(list);
    } catch (error) {
      console.log(error);
    }
  };

  const editHandler = (idObat) => {
    let id = idObat;
    try {
      navigate(`/item/${id}`);
    } catch (error) {
      alert(error)
    }
  };
  const deleteHandler = async (idObat, namaObat) => {
    let id = idObat;
    console.log(id);
    console.log(authToken);
    if (
      window.confirm(`Apakah anda mau menghapus data obat ${namaObat}`) == true
    ) {
      try {
        const hapus = await api.delete(`api/v1/medicine/${id}`, {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        });
        alert(`Obat ${namaObat} berhasil dihapus`);
      } catch (error) {
        alert(`Obat ${namaObat} gagal dihapus`);
        console.log(error);
      }
    } else {
      alert("Obat tidak dihapus");
    }
  };

  useEffect(() => {
    listing();
  }, []);

  return (
    <>
      <div class="container" style={{ marginTop: "20px" }}>
        <div class="row">
          {dataMeds.map((obat, i) =>
            i % 2 == 0 ? (
              <div class="col-md-5" style={columnStyle} key={i}>
                <div style={imagePositionStyle}>
                  <img style={imageStyle} src={obat.imageURL} loading='lazy' />
                </div>
                <p style={titleMedicineStyle}>{obat.name}</p>
                <p style={paragraphStyle}>{obat.priceString}</p>
                <p style={paragraphStyle}>QTY: {obat.quantity}</p>
                <button
                  class="btn btn-info"
                  type="button"
                  style={paragraphStyle}
                  onClick={()=>{
                    editHandler(obat.id)
                  }}
                >
                  Edit
                </button>
                <button
                  class="btn btn-danger"
                  type="button"
                  style={paragraphStyle}
                  onClick={() => {
                    deleteHandler(obat.id, obat.name);
                  }}
                >
                  Delete
                </button>
              </div>
            ) : (
              <>
                <div class="col-md-5" style={columnStyle} key={i}>
                  <div style={imagePositionStyle}>
                    <img style={imageStyle} src={obat.imageURL} loading='lazy' />
                  </div>
                  <p style={titleMedicineStyle}>{obat.name}</p>
                  <p style={paragraphStyle}>{obat.priceString}</p>
                  <p style={paragraphStyle}>QTY: {obat.quantity}</p>
                  <button
                    class="btn btn-info"
                    type="button"
                    style={paragraphStyle}
                    onClick={()=>{
                      editHandler(obat.id);
                    }}
                  >
                    Edit
                  </button>
                  <button
                    class="btn btn-danger"
                    type="button"
                    style={paragraphStyle}
                    onClick={() => {
                      deleteHandler(obat.id, obat.name);
                    }}
                  >
                    Delete
                  </button>
                </div>
                <div class="w-100"></div>
              </>
            )
          )}
        </div>
      </div>
    </>
  );
};
export default ListData;
