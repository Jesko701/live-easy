import React from "react";
import { CircleLoader } from "react-spinners";
const Spinner = () => {
  return (
    <div class="d-flex justify-content-center" >
      <CircleLoader color="#ff0000" size={100} loading speedMultiplier={2} />
    </div>
  );
};

export default Spinner;
