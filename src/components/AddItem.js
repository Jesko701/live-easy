import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../Auth/Auth";
import { api } from "./config";
import Spinner from "./Spinner";

const AddItem = () => {
  const divStyle = {
    marginTop: "20px",
  };

  const promptStyle = {
    fontSize: "15px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  };

  const inputStyle = {
    height: "30px",
    width: "100%",
    borderRadius: "20px",
    padding: "10px",
  };
  const { authToken } = useAuth();
  const navigate = useNavigate();
  const [dataMedicine, setDataMedicine] = useState({
    name: "",
    price: null,
    quantity: null,
    image: null,
  });
  const [isSpin, setSpin] = useState(false);

  const nama = useRef();
  const harga = useRef();
  const jumlah = useRef();
  const url_picture = useRef();

  const addDataIntoUseState = () => {
    let data = dataMedicine;
    data["name"] = nama.current.value;
    data["price"] = harga.current.value;
    data["quantity"] = jumlah.current.value;
    data["image"] = url_picture.current.files[0];
    setDataMedicine(data);
  };

  const submitHandler = async () => {
    console.log(authToken);
    console.log(dataMedicine);
    console.log(dataMedicine["image"]);
    setSpin(true)
    addDataIntoUseState();
    const bodyFormData = new FormData();
    bodyFormData.append("name", dataMedicine.name);
    bodyFormData.append("price", dataMedicine.price);
    bodyFormData.append("quantity", dataMedicine.quantity);
    bodyFormData.append("image", dataMedicine.image);
    console.log(bodyFormData);
    if (window.confirm("Apakah anda ingin menginput?") == true) {
      try {
        const medicine = await api.post("api/v1/medicine", bodyFormData, {
          headers: {
            Authorization: `Bearer ${authToken}`,
            "Content-Type": "multipart/form-data",
          },
        });
        console.log(medicine);
        setTimeout(()=>{
          navigate("/item");
        },1000)
        alert(`Data berhasil diinput`);
      } catch (error) {
        alert("Gagal menginput data");
        console.log(error);
      }
    } else {
      alert("Abort input data");
    }

  };

  const showFeedback = () => {
    if (isSpin) return <Spinner />;
  };

  return (
    <>
      {showFeedback()}
      <div id="Drugs-Name" style={divStyle}>
        <p style={promptStyle}>Drugs Name</p>
        <input type="text" id="name" style={inputStyle} ref={nama} />
      </div>
      <div id="Quantity" style={divStyle}>
        <p style={promptStyle}>Quantity</p>
        <input type="number" id="quantity" style={inputStyle} ref={jumlah} />
      </div>
      <div id="Price" style={divStyle}>
        <p style={promptStyle}>Price</p>
        <input type="number" id="price" style={inputStyle} ref={harga} />
      </div>
      <div id="Images" style={divStyle}>
        <p style={promptStyle}>Image</p>
        <input type="file" accept="image/*" ref={url_picture} />
      </div>
      <div id="button" style={{ textAlign: "center", marginTop: "17px" }}>
        <button
          onClick={submitHandler}
          class="btn btn-primary"
          type="button"
          style={{
            fontSize: "25px",
            minWidth: "119px",
            background: "#518ef8",
            fontFamily: "Poppins, sans-serif",
            fontWeight: "bold",
            padding: "10px",
          }}
        >
          Submit
          <br />
        </button>
      </div>
    </>
  );
};

export default AddItem;
