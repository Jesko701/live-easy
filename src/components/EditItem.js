import { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { api } from "./config";
import { useAuth } from "../Auth/Auth";
import Spinner from "./Spinner";

const EditItem = () => {
  const { id } = useParams();
  const { authToken } = useAuth();
  const navigate = useNavigate();

  const divStyle = {
    marginTop: "20px",
  };

  const promptStyle = {
    fontSize: "15px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  };

  const inputStyle = {
    height: "30px",
    width: "100%",
    borderRadius: "20px",
    padding: "10px",
  };

  const ImageDiv = {
    ...inputStyle,
    textAlign: "center",
    marginBottom: "10rem",
  };

  const [medicine, setMedicine] = useState({});
  const [spin, setSpin] = useState(false);

  const nama = useRef();
  const jumlah = useRef();
  const harga = useRef();

  const updateDataUseState = () => {
    const newMedicine = medicine;
    newMedicine["name"] = nama.current.value;
    newMedicine["price"] = harga.current.value;
    newMedicine["quantity"] = jumlah.current.value;
    setMedicine(newMedicine);
  };

  const getOneMedicine = async () => {
    try {
      const get = await api.get(`api/v1/medicine/${id}`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      });
      setMedicine(get.data.data);
    } catch (error) {
      console.log(error.response);
    }
  };

  const updateMedicine = async () => {
    setSpin(true);
    const bodyFormData = new FormData();
    try {
      if (window.confirm(`apakah anda mau mengupdate obat`) === true) {
        updateDataUseState();
        bodyFormData.append("name", medicine.name);
        bodyFormData.append("price", medicine.price);
        bodyFormData.append("quantity", medicine.quantity);
        const update = await api.put(`api/v1/medicine/${id}`, bodyFormData, {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        });
        console.log(update);
        setTimeout(()=>{
          alert(`Obat ${medicine.name} berhasil di update`)
          navigate('/item')
        },1000)
      } else {
        alert("Tidak mengupdate obat");
      }
    } catch (error) {
      console.log(error.response);
    }
    setTimeout(() => {
      setSpin(false);
    }, 1000);
  };

  const showSpinner = () => {
    if (spin) return <Spinner/>
  }

  useEffect(() => {
    getOneMedicine();
  }, []);

  return (
    <>
      {showSpinner()}
      <div style={ImageDiv}>
        <img
          style={{ width: "30%", height: "fit-content" }}
          src={medicine.imageURL}
          loading='lazy'
        ></img>
      </div>
      <div id="Drugs-Name" style={divStyle}>
        <p style={promptStyle}>Drugs Name</p>
        <input
          type="text"
          id="name"
          style={inputStyle}
          defaultValue={medicine.name}
          ref={nama}
        />
      </div>
      <div id="Quantity" style={divStyle}>
        <p style={promptStyle}>Quantity</p>
        <input
          type="number"
          id="quantity"
          style={inputStyle}
          defaultValue={medicine.quantity}
          ref={jumlah}
        />
      </div>
      <div id="Price" style={divStyle}>
        <p style={promptStyle}>Price</p>
        <input
          type="number"
          id="price"
          style={inputStyle}
          defaultValue={medicine.price}
          ref={harga}
        />
      </div>
      <div id="button" style={{ textAlign: "center", marginTop: "17px" }}>
        <button
          class="btn btn-primary"
          type="button"
          style={{
            fontSize: "25px",
            minWidth: "119px",
            background: "#518ef8",
            fontFamily: "Poppins, sans-serif",
            fontWeight: "bold",
            padding: "10px",
          }}
          onClick={updateMedicine}
        >
          Submit
          <br />
        </button>
      </div>
    </>
  );
};

export default EditItem;
